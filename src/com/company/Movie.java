package com.company;

import java.util.ArrayList;
import java.util.List;

public class Movie {
    private final String title;
    private final String studio;
    private final String rating;

    Movie(String titleOfTheMovie, String studioOfTheMovie, String ratingOfTheMovie){
        title = titleOfTheMovie;
        studio = studioOfTheMovie;
        rating = ratingOfTheMovie;
    }

    Movie(String titleOfTheMovie, String studioOfTheMovie){
        title = titleOfTheMovie;
        studio = studioOfTheMovie;
        rating = "PG";
    }

    public String getRating(){
        return rating;
    }

    public String getStudio(){
        return studio;
    }

    public String getTitle(){
        return title;
    }

    public static Movie[] getPg(Movie[] arrayOfMovies){
        List<Movie> moviesRatedPg = new ArrayList<>();

        for(Movie movie : arrayOfMovies){
            if(movie.getRating().equals("PG")){
                moviesRatedPg.add(movie);
            }
        }

        int lengthOfArray = moviesRatedPg.toArray().length;
        Movie[] arrayOfMoviesRatedPG = new Movie[lengthOfArray];


        arrayOfMoviesRatedPG = moviesRatedPg.toArray(arrayOfMoviesRatedPG);

        return arrayOfMoviesRatedPG;
    }

    

    public static void main(String[] args){
        Movie casinoRoyal = new Movie("Casino Royal", "Eon Productions", "PG-13");
        Movie harryPotter = new Movie("Harry Potter", "WB", "PG");
        Movie hungerGames = new Movie("Hunger Games", "WB", "NOT PG");

        Movie[] arrayOfMovies = {casinoRoyal, harryPotter, hungerGames};

        Movie[] moviesRatedPg = getPg(arrayOfMovies);

        for(Movie movie : moviesRatedPg){
            System.out.println(movie.getTitle() + " " + movie.getStudio());
        }
    }
}
